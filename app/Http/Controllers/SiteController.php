<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Counter;
use App\Device;
use App\Chanel;

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$counters = Counter::all();
		return view('index')->with('counters', $counters);
	}

	public function SetCounter(Request $request) {
		$counter = Counter::where('name', $request->input('name'))->first();
		$counter->value = $request->input('value');
		$counter->save();
		return json_encode('ok');
	}

	public function Devices() {
		$devices = Device::all();
		return view('devices')->with('devices', $devices);
	}

	public function SetDevice(Request $request) {
		$device = Device::where('name', $request->input('name'))->first();
		$device->status = $request->input('value');
		$device->save();
		return json_encode('ok');
	}

	public function Tv() {
		$chanels = Chanel::all();
		return view('tv')->with('chanels', $chanels);
	}

	public function SetTv(Request $request) {
		$chanel = Chanel::where('active', 1)->first();
		$chanel->active = 0;
		$chanel->save();

		$chanel = Chanel::where('name', $request->input('name'))->first();
		$chanel->active = 1;
		$chanel->save();

		return json_encode('ok');
	}
}
