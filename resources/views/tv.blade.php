@extends('layout.neon')

@section('content')

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 tv-items">
		@foreach ($chanels as $item)
			<button class="btn btn-primary {{ $item->active == 1 ? 'active' : '' }}" chanel='{{ $item->name }}'>{!! $item->label !!}</button>
		@endforeach
	</div>
</div>
@endsection
