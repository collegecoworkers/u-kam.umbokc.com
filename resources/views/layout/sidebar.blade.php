<div class="sidebar-menu">
	<div class="sidebar-menu-inner">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo">
				<a href="{{ URL::to('/') }}">
					Smart House
				</a>
			</div>
			<div class="sidebar-mobile-menu visible-xs">
				<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>
		<ul id="main-menu" class="main-menu">
			<!-- add class "multiple-expanded" to allow multiple submenus to open -->
			<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
			<li class="{{ Request::path() == '/' ? 'active' : ''}}">
				<a href="{{ URL::to('/') }}">
					<i class="glyphicon glyphicon-dashboard"></i>
					<span class="title">Счетчики</span>
				</a>
			</li>
			<li class="{{ Request::path() == 'devices' ? 'active' : ''}}">
				<a href="{{ URL::to('/devices') }}">
					<i class="glyphicon glyphicon-off"></i>
					<span class="title">Устройства</span>
				</a>
			</li>
			<li class="{{ Request::path() == 'tv' ? 'active' : ''}}">
				<a href="{{ URL::to('/tv') }}">
					<i class="glyphicon glyphicon-signal"></i>
					<span class="title">TV</span>
				</a>
			</li>
		</ul>
	</div>
</div>