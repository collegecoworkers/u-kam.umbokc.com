@extends('layout.neon')

@section('content')

<div class="row">
	@foreach ($devices as $item)
		<div class="col-md-3 col-sm-6 col-xs-12 device-item">
			<div class="panel" device='{{ $item->name }}'>
				<button>
					<i class="fa fa-{{ $item->icon }}"></i>
					<span>{{ $item->label }}</span>
				</button>
				@if ($item->status == 1)
					<div class="status on">on</div>
				@else
					<div class="status">off</div>
				@endif
			</div>
		</div>
	@endforeach

</div>
@endsection