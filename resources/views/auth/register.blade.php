@extends('layout.auth')

@section('content')
<div class="login-container">

	<div class="login-header login-caret" pt=5em pb=5em>
		<h1 c#f ta:c>
			{{ config('app.name', 'Laravel') }} Регистрация
		</h1>
	</div>

	<div class="login-form">
		
		<div class="login-content">
			
			<form method="post" action="{{ route('register') }}">
				{{ csrf_field() }}

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user-add"></i>
						</div>
						<input type="text" class="form-control" name="name" id="username" placeholder="Ваше имя" data-mask="[a-zA-Z0-9\.]+" required data-is-regex="true" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-mail"></i>
						</div>

						<input type="text" class="form-control" name="email" id="email" required="" placeholder="E-mail" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-lock"></i>
						</div>

						<input required="" type="password" class="form-control" name="password" id="password" placeholder="Выберите пароль" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-lock"></i>
						</div>

						<input required="" type="password" class="form-control" name="password_confirmation" id="password" placeholder="Повторите пароль" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-success btn-block btn-login">
						Завершить регистрацию
					</button>
				</div>

			</form>


			<div class="login-bottom-links">

				<a href="{{ route('login') }}" class="link">
					<i class="entypo-lock"></i>
					Авторизация
				</a>
			</div>
		</div>
	</div>
</div>
@endsection
