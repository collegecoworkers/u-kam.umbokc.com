@extends('layout.auth')

@section('content')
<div class="login-container">

    <div class="login-header login-caret" pt=5em pb=5em>
        <h1 c#f ta:c>
            {{ config('app.name', 'Laravel') }} Войти
        </h1>
    </div>

    <div class="login-form">

        <div class="login-content">

            <form method="post" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-mail"></i>
                        </div>

                        <input type="text" class="form-control" name="email" id="email" required="" placeholder="E-mail" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-lock"></i>
                        </div>

                        <input required="" type="password" class="form-control" name="password" id="password" placeholder="Пароль" autocomplete="off" />
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block btn-login">
                        Войти
                    </button>
                </div>

            </form>


            <div class="login-bottom-links">

                <a href="{{ route('register') }}" class="link">
                    <i class="entypo-lock"></i>
                    Зарегистрироваться
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
