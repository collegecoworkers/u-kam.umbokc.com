@extends('layout.neon')

@section('content')

<div class="row">
	@foreach ($counters as $item)
		<div class="col-md-3 col-sm-6 col-xs-12 {{ $item->name }} progress-item"
			counter='{{ $item->name }}' value='{{ $item->value }}' icon='{{ $item->icon }}'
			{!! $item->editable == 1 ? '' : 'block="true"' !!}
			{!! $item->text == '' ? '' : ('text="' . $item->text . '"') !!}
			ta:c m:v>
			<div class="progress" m:h:a>
			</div>
			<h3>{{ $item->label }}</h3>
			@if ($item->editable == 1)
				<input type="range" min="0" max="1" step="0.01" value="0" data-rangeslider>
				<output id="js-output"></output>
			@endif
		</div>
	@endforeach
</div>
@endsection