<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('Smart house');
Route::post('counter', 'SiteController@SetCounter');
Route::get('/devices', 'SiteController@Devices');
Route::post('device', 'SiteController@SetDevice');
Route::get('/tv', 'SiteController@Tv');
Route::post('tv', 'SiteController@SetTv');
